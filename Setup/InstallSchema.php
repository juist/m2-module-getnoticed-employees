<?php

namespace GetNoticed\Employees\Setup;

use GetNoticed\Employees\Model\ResourceModel\Employee;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table as DdlTable;

/**
 * Class InstallSchema
 *
 * @package GetNoticed\Employees\Setup
 */
class InstallSchema
    implements \Magento\Framework\Setup\InstallSchemaInterface
{

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        // Begin setup
        $setup->startSetup();

        // Add employees table
        $this->createEmployeesTable($setup, $setup->getConnection(), $context);

        // Finish setup
        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function createEmployeesTable(
        SchemaSetupInterface $setup,
        AdapterInterface $adapter,
        ModuleContextInterface $context
    ) {
        if ($setup->tableExists(Employee::TABLE_NAME) === false) {
            $table = $adapter
                ->newTable(Employee::TABLE_NAME)
                ->addColumn(
                    'employee_id',
                    DdlTable::TYPE_INTEGER,
                    null,
                    [
                        'primary'  => true,
                        'unsigned' => true,
                        'identity' => true,
                        'nullable' => false
                    ],
                    'Employee Id'
                )
                ->addColumn(
                    'prefix',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'Prefix'
                )
                ->addColumn(
                    'first_name',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false
                    ],
                    'First Name'
                )
                ->addColumn(
                    'middle_name',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'Middle Name'
                )
                ->addColumn(
                    'last_name',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false
                    ],
                    'Last Name'
                )
                ->addColumn(
                    'suffix',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'Suffix'
                )
                ->addColumn(
                    'initials',
                    DdlTable::TYPE_TEXT,
                    50,
                    [
                        'nullable' => true
                    ],
                    'Initials'
                )
                ->addColumn(
                    'role',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'Role'
                )
                ->addColumn(
                    'department',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'Department'
                )
                ->addColumn(
                    'photo',
                    DdlTable::TYPE_TEXT,
                    1024,
                    [
                        'nullable' => true
                    ],
                    'Photo'
                )
                ->addColumn(
                    'email',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'E-mail address'
                )
                ->addColumn(
                    'social_media',
                    DdlTable::TYPE_TEXT,
                    null,
                    [
                        'nullable' => true
                    ],
                    'Social Media'
                )
                ->addColumn(
                    'street_name',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'Street name'
                )
                ->addColumn(
                    'house_number',
                    DdlTable::TYPE_TEXT,
                    10,
                    [
                        'nullable' => true
                    ],
                    'House number'
                )
                ->addColumn(
                    'house_number_addition',
                    DdlTable::TYPE_TEXT,
                    10,
                    [
                        'nullable' => true
                    ],
                    'House number addition'
                )
                ->addColumn(
                    'postcode',
                    DdlTable::TYPE_TEXT,
                    25,
                    [
                        'nullable' => true
                    ],
                    'Postcode'
                )
                ->addColumn(
                    'state',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'State'
                )
                ->addColumn(
                    'country_id',
                    DdlTable::TYPE_TEXT,
                    2,
                    [
                        'nullable' => true
                    ],
                    'Country Id'
                )
                ->addForeignKey(
                    $adapter->getForeignKeyName(
                        Employee::TABLE_NAME,
                        'country_id',
                        $setup->getTable('directory_country'),
                        'country_id'
                    ),
                    'country_id',
                    $setup->getTable('directory_country'),
                    'country_id',
                    DdlTable::ACTION_CASCADE
                )
                ->addColumn(
                    'telephone_number',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'Telephone number'
                )
                ->addColumn(
                    'mobile_phone_number',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'Mobile phone number'
                )
                ->addColumn(
                    'fax_number',
                    DdlTable::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true
                    ],
                    'Fax Number'
                )
                ->addColumn(
                    'short_description',
                    DdlTable::TYPE_TEXT,
                    null,
                    [
                        'nullable' => true
                    ],
                    'Short description'
                )
                ->addColumn(
                    'description',
                    DdlTable::TYPE_TEXT,
                    null,
                    [
                        'nullable' => true
                    ],
                    'Description'
                );
            $adapter->createTable($table);
        }
    }

}