<?php

namespace GetNoticed\Employees\Model\ResourceModel\Employee;

use GetNoticed\Employees\Helper\DataHelper;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class Collection
 *
 * @package GetNoticed\Employees\Model\ResourceModel\Employee
 */
class Collection
    extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'employee_id';

    /**
     * @var \GetNoticed\Employees\Helper\DataHelper
     */
    protected $employeeDataHelper;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @inheritDoc
     */
    public function __construct(
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \GetNoticed\Employees\Helper\DataHelper $employeeDataHelper,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->jsonSerializer = $jsonSerializer;
        $this->employeeDataHelper = $employeeDataHelper;

        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    /**
     * Construct collection model
     */
    protected function _construct()
    {
        $this->_init(
            \GetNoticed\Employees\Model\Employee::class,
            \GetNoticed\Employees\Model\ResourceModel\Employee::class
        );
    }

    /**
     * @inheritDoc
     */
    protected function _afterLoad()
    {
        foreach ($this->getItems() as $employee) {
            /** @var \GetNoticed\Employees\Model\Employee $employee */
            $employee->setName($this->employeeDataHelper->getEmployeeName($employee));
            $employee->setAddress($this->employeeDataHelper->getEmployeeAddress($employee));
            try {
                $employee->setData(
                    'social_media', $this->jsonSerializer->unserialize($employee->getData('social_media'))
                );
            } catch (\Exception $e) {
                $employee->setData('social_media', []);
            }
        }

        return parent::_afterLoad();
    }

}