<?php

namespace GetNoticed\Employees\Model\ResourceModel;

/**
 * Class Employee
 *
 * @package GetNoticed\Employees\Model\ResourceModel
 */
class Employee
    extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    const TABLE_NAME = 'getnoticed_employees';

    /**
     * Construct resource model
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, 'employee_id');
    }

}