<?php

namespace GetNoticed\Employees\Model;

use GetNoticed\Employees\Api\Data\EmployeeInterface;
use GetNoticed\Employees\Data\ValidationResultsFactory;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Employee
 *
 * @package GetNoticed\Employees\Model
 *
 * @method \GetNoticed\Employees\Model\ResourceModel\Employee getResource()
 * @method \GetNoticed\Employees\Model\ResourceModel\Employee\Collection getCollection()
 */
class Employee
    extends \Magento\Framework\Model\AbstractModel
    implements \GetNoticed\Employees\Api\Data\EmployeeInterface,
               \Magento\Framework\DataObject\IdentityInterface
{

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $countryFactory;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Country
     */
    protected $countryResource;

    /**
     * @var \Magento\Directory\Model\Country
     */
    private $country;

    /**
     * @var ValidationResultsFactory
     */
    protected $validationResultsFactory;

    /**
     * @var array
     */
    protected $photo;

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var \GetNoticed\Employees\Model\ImageUploader
     */
    protected $imageUploader;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerializer;

    /**
     * @inheritDoc
     */
    public function __construct(
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \GetNoticed\Employees\Model\ImageUploader $imageUploader,
        \Magento\Framework\Filesystem $filesystem,
        StoreManagerInterface $storeManager,
        UrlInterface $url,
        ValidationResultsFactory $validationResultsFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\ResourceModel\Country $countryResource,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->jsonSerializer = $jsonSerializer;
        $this->imageUploader = $imageUploader;
        $this->filesystem = $filesystem;
        $this->storeManager = $storeManager;
        $this->url = $url;
        $this->validationResultsFactory = $validationResultsFactory;
        $this->countryFactory = $countryFactory;
        $this->countryResource = $countryResource;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Set cache tag
     */
    const CACHE_TAG = 'getnoticed_employees_employee';

    /**
     * @var string
     */
    protected $_cacheTag = 'getnoticed_employees_employee';

    /**
     * @var string
     */
    protected $_eventPrefix = 'getnoticed_employees_employee';

    /**
     * Construct model
     */
    protected function _construct()
    {
        $this->_init(\GetNoticed\Employees\Model\ResourceModel\Employee::class);
    }

    /**
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->getData('name');
    }

    /**
     * @param string $name
     *
     * @return \GetNoticed\Employees\Model\Employee
     */
    public function setName(string $name): Employee
    {
        return $this->setData('name', $name);
    }

    /**
     * @inheritDoc
     */
    public function getPrefix(): string
    {
        return $this->getData('prefix') ?: '';
    }

    /**
     * @return string
     */
    public function getInitials(): string
    {
        return $this->getData('initials') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getFirstName(): string
    {
        return $this->getData('first_name');
    }

    /**
     * @inheritDoc
     */
    public function getMiddleName(): string
    {
        return $this->getData('middle_name') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getLastName(): string
    {
        return $this->getData('last_name');
    }

    /**
     * @inheritDoc
     */
    public function getSuffix(): string
    {
        return $this->getData('suffix') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getRole(): string
    {
        return $this->getData('role') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getDepartment(): string
    {
        return $this->getData('department') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getPhoto()
    {
        if ($this->getData('photo') !== null && $this->getData('photo') !== '') {
            $fileName = $this->getData('photo');
            $path = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath()
                . 'employee' . DIRECTORY_SEPARATOR . $fileName;
            $url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
                . 'employee' . DIRECTORY_SEPARATOR . $fileName;

            if ($this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
                             ->isExist('employee' . DIRECTORY_SEPARATOR . $fileName)
            ) {
                return [
                    'name'     => $fileName,
                    'url'      => $url,
                    'type'     => 'image',
                    'size'     => \filesize($path),
                    'existing' => true
                ];
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getImageData(): array
    {
        $itemData = [];

        if (is_array($this->getPhoto())) {
            $itemData['photo'] = [
                $this->getPhoto()
            ];
        }

        return $itemData;
    }

    /**
     * @inheritDoc
     */
    public function setPhoto(string $path): EmployeeInterface
    {
        return $this->setData('photo', $path);
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->getData('email') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getSocialMediaAccounts(): array
    {
        throw new \Exception('To implement'); // @todo
    }

    /**
     * @inheritDoc
     */
    public function getAddress(): string
    {
        return $this->getData('address_line');
    }

    /**
     * @param string $address
     *
     * @return string
     */
    public function setAddress(string $address): Employee
    {
        return $this->setData('address_line', $address);
    }

    /**
     * @inheritDoc
     */
    public function getStreetName(): string
    {
        return $this->getData('street_name') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getHouseNumber(): string
    {
        return $this->getData('house_number') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getHouseNumberAddition(): string
    {
        return $this->getData('house_number_addition') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getPostcode(): string
    {
        return $this->getData('postcode') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getState(): string
    {
        return $this->getData('state') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getCountry(): \Magento\Directory\Model\Country
    {
        if ($this->country === null && $this->hasData('country_id')) {
            $this->country = $this->countryFactory->create();
            $this->countryResource->loadByCode($this->country, $this->getData('country_id'));

            throw new \Exception('Implementation required: check if loaded properly or throw exception');
        }

        return $this->country;
    }

    /**
     * @inheritDoc
     */
    public function getTelephone(): string
    {
        return $this->getData('telephone_number') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getMobile(): string
    {
        return $this->getData('mobile_phone_number') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getFax(): string
    {
        return $this->getData('fax_number') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getShortDescription(): string
    {
        return $this->getData('short_description') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return $this->getData('description') ?: '';
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): \DateTime
    {
        return new \DateTime($this->getData('created_at'));
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): \DateTime
    {
        return new \DateTime($this->getData('updated_at'));
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function setDataFromPost(array $data = [], $validateOnly = false): Employee
    {
        // This data is automatically generated, so don't add it.
        unset($data['name'], $data['address']);

        // If a photo was uploaded, move it to the proper directory and update information
        if (array_key_exists('photo', $data) === true) {
            if (is_array($data['photo']) && count($data['photo']) >= 1) {
                $photoData = current($data['photo']);

                if (array_key_exists('existing', $photoData) && 'true' === $photoData['existing']) {
                    $photoName = $photoData['name'];
                    $data['photo'] = $photoName;
                } else {
                    // New upload
                    $photoName = $photoData['name'];
                    $data['photo'] = $photoName;

                    // Move uploaded file
                    if ($validateOnly === false) {
                        $this->imageUploader->moveFileFromTmp($photoName);
                    }
                }
            }
        } else {
            $data['photo'] = '';
        }

        return $this->setData($data);
    }

    /**
     * @return \GetNoticed\Employees\Api\Data\ValidationResultsInterface
     */
    public function validate(): \GetNoticed\Employees\Api\Data\ValidationResultsInterface
    {
        $validationResults = $this->validationResultsFactory->create();

        try {
            // Validate required fields
            if ($this->getFirstName() === null || $this->getFirstName() === '') {
                throw new \Exception('First name is required');
            }

            if ($this->getLastName() === null || $this->getLastName() === '') {
                throw new \Exception('Last name is required');
            }

            // Allow other modules to validate us as well.
            $this->_eventManager->dispatch(
                'getnoticed_employees_employee_validate',
                [
                    'employee' => $this
                ]
            );

            // Validate optional fields
            $socialMedia = $this->getData('social_media');
            if (is_array($this->getData('social_media'))) {
                $socialMedia = new \Magento\Framework\DataObject($socialMedia);

                // Validate social media separately for ease of programming.
                $this->_eventManager->dispatch(
                    'getnoticed_employees_employee_validate_social_media',
                    [
                        'employee'     => $this,
                        'social_media' => $socialMedia
                    ]
                );
            }

            // Return results
            $validationResults
                ->setIsValid(true)
                ->setMessages([]);
        } catch (\Exception $e) {
            $validationResults
                ->setIsValid(false)
                ->setMessages([$e->getMessage()]);
        }

        return $validationResults;
    }

    /**
     * @inheritDoc
     */
    public function afterLoad()
    {
        try {
            $this->setData('social_media', $this->jsonSerializer->unserialize($this->getData('social_media')));
        } catch (\Exception $e) {
            $this->setData('social_media', []);
        }

        return parent::afterLoad();
    }

    /**
     * @inheritDoc
     */
    public function beforeSave()
    {
        try {
            $this->setData('social_media', $this->jsonSerializer->serialize($this->getData('social_media')));
        } catch (\Exception $e) {
            $this->setData('social_media', '[]');
        }

        return parent::beforeSave();
    }


}
