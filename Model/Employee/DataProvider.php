<?php

namespace GetNoticed\Employees\Model\Employee;

use Magento\Backend\Model\Session;

/**
 * Class DataProvider
 *
 * @package GetNoticed\Employees\Model\Employee
 */
class DataProvider
    extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * @var \GetNoticed\Employees\Model\ResourceModel\Employee\CollectionFactory
     */
    protected $employeeCollectionFactory;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $session;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     */
    protected $jsonSerializer;


    /**
     * DataProvider constructor.
     *
     * @param \Magento\Framework\Serialize\Serializer\Json                         $jsonSerializer
     * @param \Magento\Backend\Model\Session                                       $session
     * @param \GetNoticed\Employees\Model\ResourceModel\Employee\CollectionFactory $employeeCollectionFactory
     * @param string                                                               $name
     * @param string                                                               $primaryFieldName
     * @param string                                                               $requestFieldName
     * @param array                                                                $meta
     * @param array                                                                $data
     */
    public function __construct(
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Backend\Model\Session $session,
        \GetNoticed\Employees\Model\ResourceModel\Employee\CollectionFactory $employeeCollectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->jsonSerializer = $jsonSerializer;
        $this->session = $session;
        $this->collection = $employeeCollectionFactory->create();

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

    }

    /**
     * @return array
     */
    public function getData()
    {
        if ($this->loadedData === null) {
            $this->loadedData = [];

            foreach ($this->getCollection()->getItems() as $employee) {
                /** @var \GetNoticed\Employees\Model\Employee $employee */
                $this->loadedData[$employee->getId()] = [
                    'employee' => array_merge(
                        $employee->getData(),
                        $employee->getImageData(),
                        [
                            'id_field_name' => $employee->getIdFieldName()
                        ]
                    )
                ];
            }
        }

        $employeeData = $this->session->getEmployeeFormData();
        if (!empty($employeeData)) {
            $employeeId = isset($data['employee']['entity_id']) ? $employeeData['employee']['entity_id'] : null;
            $this->loadedData[$employeeId] = $employeeData;
            $this->session->unsEmployeeFormData();
        }

        return $this->loadedData;
    }

}