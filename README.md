# Get.Noticed Employees module
This module allows a shop administrator to add information about his employees to his Magento webshop.

By default, these fields are supported:
* Name (prefix, initials, first name, middle name, last name, suffix)
* Role
* Department
* Photo
* E-mail address
* Social media accounts (Facebook, YouTube, Twitter)
* Street name
* House number
* Postcode
* State
* Country
* Telephone number
* Mobile number
* Fax number
* Short description
* Description