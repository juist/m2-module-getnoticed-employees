<?php

namespace GetNoticed\Employees\Test\Unit\Observers;

use \GetNoticed\Employees\Observers\ValidateBaseSocialMedia;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class ValidateBaseSocialMediaTest
 *
 * @package GetNoticed\Employees\Test\Unit\Observers
 */
class ValidateBaseSocialMediaTest
    extends \PhpUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManager;

    /**
     * @var ValidateBaseSocialMedia
     */
    protected $validateBaseSocialMedia;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        // Helper Context
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->validateBaseSocialMedia = $this->objectManager->getObject(ValidateBaseSocialMedia::class);
    }

    /**
     * Check our test env
     */
    public function testEnv()
    {
        // Test Object Manager
        $this->assertNotNull($this->objectManager, 'Object Manager was not properly initialized');
        $this->assertInstanceOf(
            \Magento\Framework\TestFramework\Unit\Helper\ObjectManager::class,
            $this->objectManager,
            'Object Manager was not properly initialized'
        );

        // Test ValidateBaseSocialMedia
        $this->assertNotNull($this->validateBaseSocialMedia, 'ValidateBaseSocialMedia was not properly initialized');
        $this->assertInstanceOf(
            ValidateBaseSocialMedia::class,
            $this->validateBaseSocialMedia,
            'ValidateBaseSocialMedia was not properly initialized'
        );
    }

    /**
     * @param string      $whatsAppNumber
     * @param string|null $expectException
     * @param string|null $expectExceptionMessage
     *
     * @dataProvider dataProviderWhatsAppNumbers
     */
    public function testCheckWhatsAppNumber(
        string $whatsAppNumber,
        $expectException = null,
        $expectExceptionMessage = null
    ) {
        // What must our function do?
        if ($expectException !== null) {
            $this->expectException($expectException);
        }

        if ($expectExceptionMessage !== null) {
            $this->expectExceptionMessage($expectExceptionMessage);
        }

        // Test function
        $this->assertTrue(
            $this->validateBaseSocialMedia->checkWhatsAppNumber($whatsAppNumber)
        );
    }

    /**
     * @return array
     */
    public function dataProviderWhatsAppNumbers(): array
    {
        return [
            ['', LocalizedException::class, ValidateBaseSocialMedia::ERROR_MSG_WHATSAPP_NUMBER_IS_REQUIRED],
            ['+31612345678', LocalizedException::class, ValidateBaseSocialMedia::ERROR_MSG_WHATSAPP_OMIT_PLUS_SIGN],
            ['0612345678', LocalizedException::class, ValidateBaseSocialMedia::ERROR_MSG_WHATSAPP_INVALID_NUMBER],
            ['612345678', LocalizedException::class, ValidateBaseSocialMedia::ERROR_MSG_WHATSAPP_INVALID_NUMBER],
            ['3168129299']
        ];
    }

    /**
     * @param string      $url
     * @param array       $allowedUrls
     * @param string|null $expectException
     * @param string|null $expectExceptionMessage
     * @param bool        $expectExceptionMessageRegex
     *
     * @dataProvider dataProviderUrls
     */
    public function testCheckIfUrlMatches(
        string $url,
        array $allowedUrls,
        $expectException = null,
        $expectExceptionMessage = null,
        bool $expectExceptionMessageRegex = false
    ) {
        // What must our function do?
        if ($expectException !== null) {
            $this->expectException($expectException);
        }

        if ($expectExceptionMessage !== null) {
            if ($expectExceptionMessageRegex) {
                $this->expectExceptionMessageRegExp($expectExceptionMessage);
            } else {
                $this->expectExceptionMessage($expectExceptionMessage);
            }
        }

        // Test function
        $this->assertTrue(
            $this->validateBaseSocialMedia->checkIfUrlMatches($url, $allowedUrls)
        );
    }

    /**
     * @return array
     */
    public function dataProviderUrls(): array
    {
        // Dummy URL arrays
        $fbUrlNok = 'http://www2.facebook.com/my.user.name';
        $fbUrlOk = 'http://facebook.com/my.user.name';
        $fbUrls = ['facebook.com', 'www.facebook.com'];
        $anyUrls = ['domain.tld'];

        // Regex to check for malformed URL's
        $malformedRegex = sprintf(
            '/^%s$/',
            strtr(ValidateBaseSocialMedia::ERROR_MSG_URL_MALFORMED, ['%1' => '(.*)'])
        );

        // Testing data
        return [
            ['', [], LocalizedException::class, ValidateBaseSocialMedia::ERROR_MSG_URL_IS_REQUIRED],
            ['my.user.name', $fbUrls, LocalizedException::class, $malformedRegex, true],
            ['//my.user.name', $fbUrls, LocalizedException::class, $malformedRegex, true],
            [$fbUrlOk, [], LocalizedException::class, ValidateBaseSocialMedia::ERROR_MSG_ALLOWED_URLS_ARE_REQUIRED],
            ['domain.tld', $anyUrls, LocalizedException::class, $malformedRegex, true],
            [$fbUrlNok, $fbUrls, LocalizedException::class, ValidateBaseSocialMedia::ERROR_MSG_URL_IS_NOT_ALLOWED],
            [$fbUrlOk, $fbUrls]
        ];
    }

}