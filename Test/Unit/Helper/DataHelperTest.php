<?php

namespace GetNoticed\Employees\Test\Unit\Helper;

/**
 * Class DataHelperTest
 *
 * @package GetNoticed\Employees\Test\Unit\Helper
 */
class DataHelperTest
    extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManager;

    /**
     * @var \GetNoticed\Employees\Helper\DataHelper
     */
    protected $dataHelper;

    /**
     * Set up env
     */
    public function setUp()
    {
        // Helper Context
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->dataHelper = $this->objectManager->getObject(\GetNoticed\Employees\Helper\DataHelper::class);
    }

    /**
     * Test if we have all the necessary classes
     */
    public function testEnv()
    {
        // Test object manager mock
        $this->assertNotNull($this->objectManager, 'Object Manager was not initialized properly');
        $this->assertInstanceOf(
            \Magento\Framework\TestFramework\Unit\Helper\ObjectManager::class,
            $this->objectManager,
            'Object Manager was not initialized properly'
        );

        // Test data helper
        $this->assertNotNull($this->dataHelper, 'Data helper was not initialized properly');
        $this->assertInstanceOf(
            \GetNoticed\Employees\Helper\DataHelper::class,
            $this->dataHelper,
            'Data helper was not initialized properly'
        );
        $this->assertTrue(method_exists($this->dataHelper, 'getEmployeeName'), 'getEmployeeName() method missing');
        $this->assertTrue(
            method_exists($this->dataHelper, 'getEmployeeAddress'), 'getEmployeeAddress() method missing'
        );
    }

    /**
     * @param        $prefix
     * @param        $initials
     * @param string $firstName
     * @param        $middleName
     * @param string $lastName
     * @param        $suffix
     * @param string $expected
     *
     * @dataProvider dataEmployeeName
     */
    public function testGetEmployeeCustomerName(
        $prefix,
        $initials,
        string $firstName,
        $middleName,
        string $lastName,
        $suffix,
        string $expected
    ) {
        /** @var \GetNoticed\Employees\Model\Employee $employeeMock */
        $employeeMock = $this->objectManager->getObject(\GetNoticed\Employees\Model\Employee::class);
        $employeeMock->setData(
            [
                'prefix'      => $prefix,
                'initials'    => $initials,
                'first_name'  => $firstName,
                'middle_name' => $middleName,
                'last_name'   => $lastName,
                'suffix'      => $suffix
            ]
        );

        // Get some data
        $name = $this->dataHelper->getEmployeeName($employeeMock);

        $this->assertNotNull($name, 'Name should never be null');
        $this->assertNotContains('  ', $name, 'Name should never contain two sequential spaces');

        // More data
        $firstCharacter = substr($name, 0, 1);
        $lastCharacter = substr($name, -1, 1);

        $this->assertNotEquals(' ', $firstCharacter, 'Invalid whitespace at start');
        $this->assertNotEquals(' ', $lastCharacter, 'Invalid whitespace at end');

        $this->assertEquals($expected, $name, 'Name is not equal to expected value');
    }

    /**
     * @return array
     */
    public function dataEmployeeName(): array
    {
        // We only need one set of test data, we'll vary with which values are set (or '' or null).
        // Note that we will not test with a null value for $firstName or $lastName, as this will already throw a TypeError
        $prefix = 'Mr.';
        $initials = 'J.S.D.';
        $firstName = 'John';
        $middleName = 'Simon Derek';
        $lastName = 'Doe';
        $suffix = 'Ph.D';

        return [
            [null, null, '', null, '', null, ''],
            [null, null, '', null, $lastName, null, ''],
            [null, null, $firstName, null, '', null, ''],
            [null, null, $firstName, null, $lastName, null, 'John Doe'],
            [null, null, $firstName, $middleName, $lastName, null, 'John Simon Derek Doe'],
            [null, null, '', $middleName, $lastName, null, ''],
            [null, null, $firstName, $middleName, '', null, ''],
            [null, $initials, $firstName, null, $lastName, null, 'J.S.D. John Doe'],
            [null, $initials, $firstName, $middleName, $lastName, null, 'J.S.D. John Simon Derek Doe'],
            [$prefix, null, $firstName, null, $lastName, null, 'Mr. John Doe'],
            [$prefix, $initials, $firstName, null, $lastName, null, 'Mr. J.S.D. John Doe'],
            [$prefix, $initials, $firstName, $middleName, $lastName, null, 'Mr. J.S.D. John Simon Derek Doe'],
            [$prefix, $initials, $firstName, $middleName, $lastName, $suffix, 'Mr. J.S.D. John Simon Derek Doe Ph.D'],
            [null, $initials, $firstName, $middleName, $lastName, $suffix, 'J.S.D. John Simon Derek Doe Ph.D'],
            [$prefix, null, $firstName, $middleName, $lastName, $suffix, 'Mr. John Simon Derek Doe Ph.D'],
            [null, null, $firstName, $middleName, $lastName, $suffix, 'John Simon Derek Doe Ph.D'],
            [null, null, $firstName, null, $lastName, $suffix, 'John Doe Ph.D'],
        ];
    }

    /**
     * @param string $streetName
     * @param string $houseNumber
     * @param        $houseNumberAddition
     * @param string $expected
     *
     * @dataProvider dataEmployeeAddress
     */
    public function testGetEmployeeAddress(
        string $streetName,
        string $houseNumber,
        $houseNumberAddition,
        string $expected
    ) {
        /** @var \GetNoticed\Employees\Model\Employee $employeeMock */
        $employeeMock = $this->objectManager->getObject(\GetNoticed\Employees\Model\Employee::class);
        $employeeMock->setData(
            [
                'street_name'           => $streetName,
                'house_number'          => $houseNumber,
                'house_number_addition' => $houseNumberAddition
            ]
        );

        // Get some data
        $address = $this->dataHelper->getEmployeeAddress($employeeMock);

        $this->assertNotNull($address, 'Address should never be null');
        $this->assertNotContains('  ', $address, 'Address should never contain two sequential spaces');

        // More data
        $firstCharacter = substr($address, 0, 1);
        $lastCharacter = substr($address, -1, 1);

        $this->assertNotEquals(' ', $firstCharacter, 'Invalid whitespace at start');
        $this->assertNotEquals(' ', $lastCharacter, 'Invalid whitespace at end');

        $this->assertEquals($expected, $address, 'Address is not equal to expected value');
    }

    /**
     * @return array
     */
    public function dataEmployeeAddress(): array
    {
        // We only need one set of test data, we'll vary with which values are set (or '' or null).
        // Note that we will not test with a null value for $streetName or $houseNumber, as this will already throw a TypeError
        $streetName = 'Some street';
        $houseNumber = '5134';
        $houseNumberAddition = 'Z';

        return [
            ['', '', null, ''],
            [$streetName, '', null, ''],
            ['', $houseNumber, null, ''],
            [$streetName, $houseNumber, null, 'Some street 5134'],
            [$streetName, $houseNumber, '', 'Some street 5134'],
            [$streetName, $houseNumber, $houseNumberAddition, 'Some street 5134Z']
        ];
    }

}