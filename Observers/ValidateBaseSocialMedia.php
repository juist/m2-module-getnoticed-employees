<?php

namespace GetNoticed\Employees\Observers;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class ValidateBaseSocialMedia
 *
 * @package GetNoticed\Employees\Observers
 */
class ValidateBaseSocialMedia
    implements ObserverInterface
{

    const ERROR_MSG_WHATSAPP_OMIT_PLUS_SIGN = 'Please omit the + and enter only the numbers of your country code, without the zeros.';
    const ERROR_MSG_WHATSAPP_NUMBER_IS_REQUIRED = 'Number is required';
    const ERROR_MSG_WHATSAPP_INVALID_NUMBER = 'Please enter your full phone number including the country code, e.g. 3161234678 for a Dutch number.';

    const ERROR_MSG_URL_IS_REQUIRED = 'URL is required';
    const ERROR_MSG_ALLOWED_URLS_ARE_REQUIRED = 'Allowed urls are required';
    const ERROR_MSG_URL_MALFORMED = 'Invalid or malformed URL: %1';
    const ERROR_MSG_URL_IS_NOT_ALLOWED = 'URL "%1" is not allowed (allowed: %2)';

    const ERROR_MSG_MALFORMED_HOST = 'Host is missing';
    const ERROR_MSG_MALFORMED_SCHEME = 'Scheme (e.g. https) is missing';

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Framework\DataObject $socialMedia */
        $socialMedia = $observer->getData('social_media');

        // Validate what data we have.
        if ($socialMedia->hasData('whatsapp') && $socialMedia->getData('whatsapp') !== '') {
            try {
                $this->checkWhatsAppNumber($socialMedia->getData('whatsapp'));
            } catch (\Exception $e) {
                throw new LocalizedException(
                    __('Could not validate the WhatsApp number: %1', $e->getMessage())
                );
            }
        }

        if ($socialMedia->hasData('facebook') && $socialMedia->getData('facebook') !== '') {
            try {
                $this->checkIfUrlMatches($socialMedia->getData('facebook'), ['facebook.com', 'www.facebook.com']);
            } catch (\Exception $e) {
                throw new LocalizedException(
                    __('Could not validate Facebook account: %1', $e->getMessage())
                );
            }
        }

        if ($socialMedia->hasData('youtube') && $socialMedia->getData('youtube') !== '') {
            try {
                $this->checkIfUrlMatches($socialMedia->getData('youtube'), ['youtube.com', 'www.youtube.com']);
            } catch (\Exception $e) {
                throw new LocalizedException(
                    __('Could not validate YouTube account: %1', $e->getMessage())
                );
            }
        }

        if ($socialMedia->hasData('linkedin') && $socialMedia->getData('linkedin') !== '') {
            try {
                $this->checkIfUrlMatches($socialMedia->getData('linkedin'), ['linkedin.com', 'www.linkedin.com']);
            } catch (\Exception $e) {
                throw new LocalizedException(
                    __('Could not validate LinkedIn account: %1', $e->getMessage())
                );
            }
        }

        if ($socialMedia->hasData('instagram') && $socialMedia->getData('instagram') !== '') {
            try {
                $this->checkIfUrlMatches($socialMedia->getData('instagram'), ['instagram.com', 'www.instagram.com']);
            } catch (\Exception $e) {
                throw new LocalizedException(
                    __('Could not validate Instagram account: %1', $e->getMessage())
                );
            }
        }

        if ($socialMedia->hasData('twitter') && $socialMedia->getData('twitter') !== '') {
            try {
                $this->checkIfUrlMatches($socialMedia->getData('twitter'), ['twitter.com', 'www.twitter.com']);
            } catch (\Exception $e) {
                throw new LocalizedException(
                    __('Could not validate Twitter account: %1', $e->getMessage())
                );
            }
        }

        if ($socialMedia->hasData('pinterest') && $socialMedia->getData('pinterest') !== '') {
            try {
                $this->checkIfUrlMatches($socialMedia->getData('pinterest'), ['pinterest.com', 'www.pinterest.com']);
            } catch (\Exception $e) {
                throw new LocalizedException(
                    __('Could not validate Pinterest account: %1', $e->getMessage())
                );
            }
        }
    }

    /**
     * @param string $whatsAppPhoneNumber
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function checkWhatsAppNumber(string $whatsAppPhoneNumber)
    {
        if (strlen($whatsAppPhoneNumber) < 1) {
            throw new LocalizedException(__(self::ERROR_MSG_WHATSAPP_NUMBER_IS_REQUIRED));
        }

        if (substr($whatsAppPhoneNumber, 0, 1) === '+') {
            throw new LocalizedException(__(self::ERROR_MSG_WHATSAPP_OMIT_PLUS_SIGN));
        }

        if (strlen((string)(int)$whatsAppPhoneNumber) < 10) {
            throw new LocalizedException(__(self::ERROR_MSG_WHATSAPP_INVALID_NUMBER));
        }

        return true;
    }

    /**
     * @param string $url
     * @param array  $allowedUrls
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function checkIfUrlMatches(string $url, array $allowedUrls)
    {
        // If URL is missing, deny
        if (strlen($url) < 1) {
            throw new LocalizedException(__(self::ERROR_MSG_URL_IS_REQUIRED));
        }

        if (count($allowedUrls) === 0) {
            throw new LocalizedException(__(self::ERROR_MSG_ALLOWED_URLS_ARE_REQUIRED));
        }

        // Parse URL to bits
        $parseUrl = parse_url($url);

        // Check if host is present
        if (!array_key_exists('host', $parseUrl)) {
            throw new LocalizedException(__(self::ERROR_MSG_URL_MALFORMED, __(self::ERROR_MSG_MALFORMED_HOST)));
        }

        if (!array_key_exists('scheme', $parseUrl)) {
            throw new LocalizedException(__(self::ERROR_MSG_URL_MALFORMED, __(self::ERROR_MSG_MALFORMED_SCHEME)));
        }

        // And check if it matches
        if (!in_array($parseUrl['host'], $allowedUrls)) {
            throw new LocalizedException(
                __(
                    self::ERROR_MSG_URL_IS_NOT_ALLOWED,
                    $parseUrl['host'],
                    $allowedUrls
                )
            );
        }

        return true;
    }

}