<?php

namespace GetNoticed\Employees\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\DataObject;


/**
 * Class Image
 *
 * @package GetNoticed\Employees\Helper
 */
class Image
    extends AbstractHelper
{

    /**
     * Current Employee
     *
     * @var \GetNoticed\Employees\Model\Employee|\Magento\Framework\DataObject
     */
    protected $_employee;

    /**
     * @var string
     */
    protected $url;

    /**
     * Reset all previous data
     *
     * @return $this
     */
    protected function _reset()
    {
        $this->_scheduleRotate = false;
        $this->_employee = null;
        $this->_imageFile = null;

        return $this;
    }

    /**
     * Initialize Helper to work with Image
     *
     * @param DataObject $employee
     * @param string     $imageId
     *
     * @return $this
     */
    public function init(DataObject $employee, $imageId)
    {
        $this->_reset();
        $this->setEmployee($employee);
        $this->setImageData();

        return $this;
    }

    /**
     * Set image data
     */
    protected function setImageData()
    {
        $this->setUrl('test');
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return Image
     */
    public function setUrl(string $url): Image
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return \GetNoticed\Employees\Model\Employee|\Magento\Framework\DataObject
     */
    public function getEmployee(): \Magento\Framework\DataObject
    {
        return $this->_employee;
    }

    /**
     * @param \GetNoticed\Employees\Model\Employee|\Magento\Framework\DataObject $employee
     *
     * @return Image
     */
    public function setEmployee(\Magento\Framework\DataObject $employee): Image
    {
        $this->_employee = $employee;

        return $this;
    }

    /**
     * Return image label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->getEmployee()->getName();
    }
}
