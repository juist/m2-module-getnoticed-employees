<?php

namespace GetNoticed\Employees\Helper;

use GetNoticed\Employees\Model\Employee;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class DataHelper
 *
 * @package GetNoticed\Employees\Helper
 */
class DataHelper
    extends AbstractHelper
{

    /**
     * @param \GetNoticed\Employees\Model\Employee $employee
     *
     * @return string
     */
    public function getEmployeeName(Employee $employee): string
    {
        // Validate necessary data
        if ($employee->getFirstName() === null || $employee->getFirstName() === '') {
            return '';
        }

        if ($employee->getLastName() === null || $employee->getLastName() === '') {
            return '';
        }

        // Add a "name" field, which is built from the prefix, first name, middle name, last name, suffix
        $nameParts = [
            $employee->getPrefix(),
            $employee->getInitials(),
            $employee->getFirstName(),
            $employee->getMiddleName(),
            $employee->getLastName(),
            $employee->getSuffix()
        ];

        // Remove empty parts
        foreach ($nameParts as $idx => $namePart) {
            if (strlen($namePart) < 1) {
                unset($nameParts[$idx]);
            }
        }

        return implode(' ', $nameParts);
    }

    /**
     * @param \GetNoticed\Employees\Model\Employee $employee
     *
     * @return string
     */
    public function getEmployeeAddress(Employee $employee): string
    {
        // Validate necessary data
        if ($employee->getStreetName() === null || $employee->getStreetName() === '') {
            return '';
        }

        if ($employee->getHouseNumber() === null || $employee->getHouseNumber() === '') {
            return '';
        }

        // Add an "address_line" field, which consists of the street name, house number + addition
        $addressParts = [
            $employee->getStreetName(),
            ' ',
            $employee->getHouseNumber(),
            $employee->getHouseNumberAddition()
        ];

        // Remove empty parts
        foreach ($addressParts as $idx => $addressPart) {
            if (strlen($addressPart) < 1) {
                unset($addressParts[$idx]);
            }
        }

        return implode($addressParts);
    }

}