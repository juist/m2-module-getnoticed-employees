<?php

/**
 * Intermediary bootstrap file. Checks if we are in /vendor/ or /app/code/.
 */
if (strpos(__DIR__, '/app/code/') !== false) {
    require '../../../../dev/tests/unit/framework/bootstrap.php';
} else {
    require '../../../dev/tests/unit/framework/bootstrap.php';
}