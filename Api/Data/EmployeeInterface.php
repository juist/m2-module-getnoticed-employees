<?php

namespace GetNoticed\Employees\Api\Data;

/**
 * Interface EmployeeInterface
 *
 * @package GetNoticed\Employees\Api\Data
 */
interface EmployeeInterface
{

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getPrefix(): string;

    /**
     * @return string
     */
    public function getInitials(): string;

    /**
     * @return string
     */
    public function getFirstName(): string;

    /**
     * @return string
     */
    public function getMiddleName(): string;

    /**
     * @return string
     */
    public function getLastName(): string;

    /**
     * @return string
     */
    public function getSuffix(): string;

    /**
     * @return string
     */
    public function getRole(): string;

    /**
     * @return string
     */
    public function getDepartment(): string;

    /**
     * @return string|array
     */
    public function getPhoto();

    /**
     * @return array
     */
    public function getImageData(): array;

    /**
     * @param string $path
     *
     * @return \GetNoticed\Employees\Api\Data\EmployeeInterface
     */
    public function setPhoto(string $path): EmployeeInterface;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return array
     */
    public function getSocialMediaAccounts(): array;

    /**
     * @return string
     */
    public function getAddress(): string;

    /**
     * @return string
     */
    public function getStreetName(): string;

    /**
     * @return string
     */
    public function getHouseNumber(): string;

    /**
     * @return string
     */
    public function getHouseNumberAddition(): string;

    /**
     * @return string
     */
    public function getPostcode(): string;

    /**
     * @return string
     */
    public function getState(): string;

    /**
     * @return \Magento\Directory\Model\Country
     */
    public function getCountry(): \Magento\Directory\Model\Country;

    /**
     * @return string
     */
    public function getTelephone(): string;

    /**
     * @return string
     */
    public function getMobile(): string;

    /**
     * @return string
     */
    public function getFax(): string;

    /**
     * @return string
     */
    public function getShortDescription(): string;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime;

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime;

    /**
     * @return \GetNoticed\Employees\Api\Data\ValidationResultsInterface
     */
    public function validate(): \GetNoticed\Employees\Api\Data\ValidationResultsInterface;

}