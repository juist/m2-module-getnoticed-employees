<?php

namespace GetNoticed\Employees\Block;

use GetNoticed\Employees\Model\ResourceModel\Employee\Collection;
use GetNoticed\Employees\Model\ResourceModel\Employee\CollectionFactory;
use Magento\Framework\View\Element\Template;

/**
 * Class EmployeesList
 *
 * @package GetNoticed\Employees\Block
 */
class EmployeesList
    extends Template
{

    /**
     * @var CollectionFactory
     */
    protected $employeeCollectionFactory;

    /**
     * @var Collection
     */
    protected $employeeCollection;

    /**
     * EmployeesList constructor.
     *
     * @param \GetNoticed\Employees\Model\ResourceModel\Employee\CollectionFactory $employeeCollectionFactory
     * @param \Magento\Framework\View\Element\Template\Context                     $context
     * @param array                                                                $data
     */
    public function __construct(
        CollectionFactory $employeeCollectionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->employeeCollectionFactory = $employeeCollectionFactory;

        parent::__construct($context, $data);
    }

    /**
     * @return \GetNoticed\Employees\Model\ResourceModel\Employee\Collection
     */
    public function getEmployeeCollection()
    {
        if ($this->employeeCollection === null) {
            $this->employeeCollection = $this->employeeCollectionFactory->create();
        }

        return $this->employeeCollection;
    }

}