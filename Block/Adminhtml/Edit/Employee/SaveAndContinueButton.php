<?php

namespace GetNoticed\Employees\Block\Adminhtml\Edit\Employee;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveAndContinueButton
 */
class SaveAndContinueButton
    extends GenericButton
    implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        $employeeId = $this->getEmployeeId();
        $data = [];
        if ($employeeId !== null) {
            $data = [
                'label'          => __('Save and Continue Edit'),
                'class'          => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit'],
                    ],
                ],
                'sort_order'     => 80,
            ];
        } else {
            $data = [
                'label'          => __('Create and Continue Edit'),
                'class'          => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit'],
                    ],
                ],
                'sort_order'     => 80,
            ];
        }

        return $data;
    }

}