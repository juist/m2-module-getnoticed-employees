<?php

namespace GetNoticed\Employees\Block\Adminhtml\Edit\Employee;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 *
 * @package GetNoticed\Employees\Block\Adminhtml\Edit\Employee
 */
class SaveButton
    extends GenericButton
    implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        $employeeId = $this->getEmployeeId();
        $data = [];
        if ($employeeId !== null) {
            $data = [
                'label'          => __('Save Employee'),
                'class'          => 'save primary',
                'data_attribute' => [
                    'mage-init' => ['button' => ['event' => 'save']],
                    'form-role' => 'save',
                ],
                'sort_order'     => 90,
            ];
        } else {
            $data = [
                'label'          => __('Create Employee'),
                'class'          => 'save primary',
                'data_attribute' => [
                    'mage-init' => ['button' => ['event' => 'save']],
                    'form-role' => 'save'
                ],
                'sort_order'     => 90
            ];
        }

        return $data;
    }
}
