<?php

namespace GetNoticed\Employees\Block\Adminhtml\Edit\Employee;

use GetNoticed\Employees\Controller\RegistryConstants;

/**
 * Class GenericButton
 *
 * @package GetNoticed\Employees\Block\Adminhtml\Edit\Employee
 */
class GenericButton
{

    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * Registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry           $registry
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
    }

    /**
     * Return the employee Id.
     *
     * @return int|null
     */
    public function getEmployeeId()
    {
        return $this->registry->registry(RegistryConstants::CURRENT_EMPLOYEE_ID);
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array  $params
     *
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }

}