<?php

namespace GetNoticed\Employees\Controller;

/**
 * Declarations of core registry keys used by the Employees module
 */
class RegistryConstants

{
    /**
     * Registry key where current employee ID is stored
     */
    const CURRENT_EMPLOYEE_ID = 'current_employee_id';

}