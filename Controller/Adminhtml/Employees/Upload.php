<?php

namespace GetNoticed\Employees\Controller\Adminhtml\Employees;

use GetNoticed\Employees\Model\ImageUploader;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Upload
 */
class Upload
    extends Action
{

    const ADMIN_RESOURCE = 'GetNoticed_Employees::employees';

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @inheritDoc
     */
    public function __construct(
        Action\Context $context,
        ImageUploader $imageUploader
    ) {
        $this->imageUploader = $imageUploader;

        parent::__construct($context);
    }

    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $imageId = $this->_request->getParam('param_name', 'image');

        try {
            $result = $this->imageUploader->saveFileToTmpDir($imageId);

            $result['cookie'] = [
                'name'     => $this->_getSession()->getName(),
                'value'    => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path'     => $this->_getSession()->getCookiePath(),
                'domain'   => $this->_getSession()->getCookieDomain(),
            ];
        } catch (\Exception $e) {
            $result = [
                'error'     => $e->getMessage(),
                'errorcode' => $e->getCode()
            ];
        }

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }

}