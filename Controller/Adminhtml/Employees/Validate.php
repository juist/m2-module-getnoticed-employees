<?php

namespace GetNoticed\Employees\Controller\Adminhtml\Employees;

use GetNoticed\Employees\Api\Data\EmployeeInterface;
use Magento\Framework\Message\Error;

/**
 * Class Validate
 *
 * @package GetNoticed\Employees\Controller\Adminhtml\Employees
 */
class Validate
    extends AbstractEmployee
{

    /**
     * Customer validation
     *
     * @param \Magento\Framework\DataObject $response
     *
     * @return EmployeeInterface|null
     */
    protected function _validateEmployee($response)
    {
        $employee = null;
        $errors = [];

        try {
            /** @var \GetNoticed\Employees\Model\Employee $employee */
            $employee = $this->employeeFactory->create();
            if (isset($this->getRequest()->getPost()['employee'])) {
                $employee->setDataFromPost($this->getRequest()->getPost()['employee'], true);
            }

            $errors = $employee->validate()->getMessages();
        } catch (\Magento\Framework\Validator\Exception $exception) {
            /* @var Error $error */
            foreach ($exception->getMessages(\Magento\Framework\Message\MessageInterface::TYPE_ERROR) as $error) {
                $errors[] = $error->getText();
            }
        }

        if ($errors) {
            $messages = $response->hasMessages() ? $response->getMessages() : [];
            foreach ($errors as $error) {
                $messages[] = $error;
            }
            $response->setMessages($messages);
            $response->setError(1);
        }

        return $employee;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        $response->setError(0);

        $employee = $this->_validateEmployee($response);

        $resultJson = $this->jsonFactory->create();
        if ($response->getError()) {
            $response->setError(true);
            $response->setMessages($response->getMessages());
        }

        $resultJson->setData($response);

        return $resultJson;
    }

}