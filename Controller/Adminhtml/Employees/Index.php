<?php

namespace GetNoticed\Employees\Controller\Adminhtml\Employees;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 *
 * @package GetNoticed\Employees\Controller\Adminhtml\Employees
 */
class Index
    extends AbstractEmployee
{

    /**
     * Resource to identify against.
     */
    const ADMIN_RESOURCE = 'GetNoticed_Employees::content_elements_employees';

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        return $this->getPage();
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    protected function getPage()
    {
        if (is_null($this->resultPage)) {
            $this->resultPage = $this->resultPageFactory->create();
            $this->_view->loadLayout();
            $this->_setActiveMenu(self::ADMIN_RESOURCE);
            $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Employees'));
            $this->_addBreadcrumb(__('Employees'), __('Employees'));
        }

        return $this->resultPage;
    }

}