<?php

namespace GetNoticed\Employees\Controller\Adminhtml\Employees;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Edit
 *
 * @package GetNoticed\Employees\Controller\Adminhtml\Employees
 */
class Edit
    extends AbstractEmployee
{

    /**
     * Resource to identify against.
     */
    const ADMIN_RESOURCE = 'GetNoticed_Employees::content_elements_employees';

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $employeeId = $this->initCurrentEmployee();

        $employeeData = [];
        $employeeData['employee'] = [];

        $employee = null;
        $isExistingEmployee = (bool)$employeeId;
        if ($isExistingEmployee) {
            try {
                $employee = $this->employeeFactory->create();
                $this->employeeResource->load($employee, $employeeId);

                if ($employee->getId() === null) {
                    throw new NoSuchEntityException(__('Employee not found'));
                }

                $employeeData['employee'] = $employee->getData();
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addException($e, __('Something went wrong while editing the employee.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('getnoticed_employees/employees/index');

                return $resultRedirect;
            }
        }
        $employeeData['employee_id'] = $employeeId;
        $this->_getSession()->setEmployeeData($employeeData);

        return $this->getPage();
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    protected function getPage()
    {
        if (is_null($this->resultPage)) {
            $this->resultPage = $this->resultPageFactory->create();
            $this->_view->loadLayout();
            $this->_setActiveMenu(self::ADMIN_RESOURCE);
            $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Edit employee'));
            $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Employees'));
            $this->_addBreadcrumb(__('Employees'), __('Employees'), 'getnoticed_employees/employees/index');
            $this->_addBreadcrumb(__('Edit employee'), __('Edit employee'));
        }

        return $this->resultPage;
    }

}