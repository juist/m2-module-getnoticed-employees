<?php

namespace GetNoticed\Employees\Controller\Adminhtml\Employees;

use GetNoticed\Employees\Controller\RegistryConstants;
use GetNoticed\Employees\Model\Employee;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 *
 * @package GetNoticed\Employees\Controller\Adminhtml\Employees
 */
class Save
    extends AbstractEmployee
{

    /**
     * Save employee action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $returnToEdit = false;
        $originalRequestData = $this->getRequest()->getPostValue();

        $employeeId = $this->getCurrentEmployeeId();

        if ($originalRequestData) {
            try {
                // Create employee object
                $currentEmployee = $this->employeeFactory->create();

                // Load from database if editing
                if ($employeeId) {
                    $this->employeeResource->load($currentEmployee, $employeeId);
                }

                // And add post data
                $employeeData = $originalRequestData['employee'];
                $currentEmployee->setDataFromPost($employeeData);

                // Validate
                $validateResult = $currentEmployee->validate();

                if ($validateResult->isValid() === false) {
                    throw new \Magento\Framework\Validator\Exception(
                        __('Failed saving the employer'),
                        null,
                        $validateResult->getMessages()
                    );
                }

                // Save employee
                $this->employeeResource->save($currentEmployee);
                $this->_getSession()->unsEmployeeFormData();

                $this->coreRegistry->register(RegistryConstants::CURRENT_EMPLOYEE_ID, $employeeId);
                $this->messageManager->addSuccess(__('You saved the employee.'));
                $returnToEdit = (bool)$this->getRequest()->getParam('back', false);
            } catch (\Magento\Framework\Validator\Exception $exception) {
                $messages = $exception->getMessages();
                if (empty($messages)) {
                    $messages = [$exception->getMessage()];
                }

                foreach ($messages as $message) {
                    $this->messageManager->addErrorMessage($message);
                }

                $this->_getSession()->setEmployeeFormData($originalRequestData);
                $returnToEdit = true;
            } catch (LocalizedException $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
                $this->_getSession()->setEmployeeFormData($originalRequestData);
                $returnToEdit = true;
            } catch (\Exception $exception) {
                $this->messageManager->addException($exception, __('Something went wrong while saving the employee.'));
                $this->_getSession()->setEmployeeFormData($originalRequestData);
                $returnToEdit = true;
            }
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($returnToEdit) {
            if ($employeeId) {
                $resultRedirect->setPath(
                    'getnoticed_employees/employees/edit',
                    ['employee_id' => $employeeId, '_current' => true]
                );
            } elseif (isset($currentEmployee) && $currentEmployee instanceof Employee && $currentEmployee->getId()) {
                $resultRedirect->setPath(
                    'getnoticed_employees/employees/edit',
                    ['employee_id' => $currentEmployee->getId(), '_currente' => true]
                );
            } else {
                $resultRedirect->setPath(
                    'getnoticed_employees/employees/create',
                    ['_current' => true]
                );
            }
        } else {
            $resultRedirect->setPath('getnoticed_employees/employees/index');
        }

        return $resultRedirect;
    }

}
