<?php

namespace GetNoticed\Employees\Controller\Adminhtml\Employees;

use GetNoticed\Employees\Controller\RegistryConstants;

/**
 * Class Delete
 *
 * @package GetNoticed\Employees\Controller\Adminhtml\Employees
 */
class Delete
    extends AbstractEmployee
{

    /**
     * Delete employee action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $employeeId = $this->getCurrentEmployeeId();
        $currentEmployee = $this->employeeFactory->create();

        // Load from database if editing
        if ($employeeId) {
            $this->employeeResource->load($currentEmployee, $employeeId);
        }

        if ($currentEmployee->getId() !== null) {
            // Save employee
            $this->employeeResource->delete($currentEmployee);
            $this->_getSession()->unsEmployeeFormData();

            $this->coreRegistry->register(RegistryConstants::CURRENT_EMPLOYEE_ID, $employeeId);
            $this->messageManager->addSuccessMessage(__('You removed the employee.'));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('getnoticed_employees/employees/index');

        return $resultRedirect;
    }

}
