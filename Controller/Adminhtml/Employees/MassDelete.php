<?php

namespace GetNoticed\Employees\Controller\Adminhtml\Employees;

use GetNoticed\Employees\Model\ResourceModel\Employee\CollectionFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 *
 * @package GetNoticed\Employees\Controller\Adminhtml\Employees
 */
class MassDelete
    extends Action
{

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Resource to identify against.
     */
    const ADMIN_RESOURCE = 'GetNoticed_Employees::content_elements_employees';

    /**
     * MassDelete constructor.
     *
     * @param \Magento\Backend\App\Action\Context                                  $context
     * @param \Magento\Ui\Component\MassAction\Filter                              $filter
     * @param \GetNoticed\Employees\Model\ResourceModel\Employee\CollectionFactory $collectionFactory
     */
    public function __construct(
        Action\Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;

        parent::__construct($context);
    }

    /**
     * Remove items
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $employee) {
            $employee->delete();
        }

        $this->messageManager->addSuccess(__('A total of %1 employee(s) have been removed.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }

}