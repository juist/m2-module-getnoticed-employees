<?php

namespace GetNoticed\Employees\Controller\Adminhtml\Employees;

use GetNoticed\Employees\Controller\RegistryConstants;
use GetNoticed\Employees\Model\EmployeeFactory;
use GetNoticed\Employees\Model\ResourceModel\Employee;
use Magento\Backend\Model\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Registry;

/**
 * Class AbstractEmployee
 *
 * @package GetNoticed\Employees\Controller\Adminhtml\Employees
 */
abstract class AbstractEmployee
    extends \Magento\Backend\App\Action
{

    /**
     * @var EmployeeFactory
     */
    protected $employeeFactory;

    /**
     * @var Employee
     */
    protected $employeeResource;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * AbstractEmployee constructor.
     *
     * @param \Magento\Backend\App\Action\Context                $context
     * @param \GetNoticed\Employees\Model\EmployeeFactory        $employeeFactory
     * @param \GetNoticed\Employees\Model\ResourceModel\Employee $employeeResource
     * @param \Magento\Framework\Registry                        $coreRegistry
     * @param \Magento\Backend\Model\View\Result\PageFactory     $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory   $jsonFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        EmployeeFactory $employeeFactory,
        Employee $employeeResource,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory
    ) {
        $this->employeeFactory = $employeeFactory;
        $this->employeeResource = $employeeResource;
        $this->coreRegistry = $coreRegistry;
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonFactory = $jsonFactory;

        parent::__construct($context);
    }

    /**
     * Initialize employee
     *
     * @return int
     */
    protected function initCurrentEmployee()
    {
        $employeeId = (int)$this->getRequest()->getParam('employee_id');

        if ($employeeId) {
            $this->coreRegistry->register(RegistryConstants::CURRENT_EMPLOYEE_ID, $employeeId);
        }

        return $employeeId;
    }

    /**
     * Retrieve current employee ID
     *
     * @return int
     */
    protected function getCurrentEmployeeId()
    {
        $originalRequestData = $this->getRequest()->getPostValue('employee');

        $employeeId = isset($originalRequestData['employee_id'])
            ? $originalRequestData['employee_id']
            : null;

        return $employeeId;
    }

}